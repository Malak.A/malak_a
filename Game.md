## Sten, Sax, Påse! 
###### Av Malak Abdulla
<hr> 
Välkommen till det klassiska spelet: Sten, sax, påse!!!

Spelet går ut på att välja ett kast som slår ut det andra.  Du spelar mot datorn. 

**Regler:**

När det är din tur väljer du mellan ett av alternativen och datorns val väljs ut slumpmässigt. 
- Väljer du sten vinner du mot sax. 
- Väljer du sax vinner du mot påse. 
- Väljer du påse vinner du mot sten.

LYCKA TILL!
