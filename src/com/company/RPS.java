package com.company;

import java.util.Random;
import java.util.Scanner;
/*
 * @author Malak.A
 *
 */

public class RPS {
    public static final String STEN = "Sten";
    public static final String SAX = "Sax";
    public static final String PÅSE = "Påse";

    public static void main(String[] args) {
        System.out.println("Välj ett slag av nedanstående alternativ: ");
        System.out.println("1. Sten");
        System.out.println("2. Sax");
        System.out.println("3. Påse");
        System.out.println();

        String humanPlayerMove = getHumanPlayerMove();
        String cpuPlayerMove = getCpuPlayerMove();

        if (humanPlayerMove.equals(cpuPlayerMove))
            System.out.println("Oavgjort! Ni valde samma slag. ");
        else if (humanPlayerMove.equals(RPS.STEN))
            System.out.println(cpuPlayerMove.equals(RPS.PÅSE) ? "Datorn vann!" : "Du vann!");
        else if (humanPlayerMove.equals(RPS.PÅSE))
            System.out.println(cpuPlayerMove.equals(RPS.SAX) ? "Datorn vann!" : "Du vann!");
        else
            System.out.println(cpuPlayerMove.equals(RPS.STEN) ? "Datorn vann!" : "Du vann!");

    }
    public static String getCpuPlayerMove() {
        String cpuplayermove;
        Random random = new Random();
        int input = random.nextInt(3) + 1;

        if (input == 1)
            cpuplayermove = RPS.STEN;
        else if (input == 2)
            cpuplayermove = RPS.PÅSE;
        else cpuplayermove = RPS.SAX;

        System.out.println("Datorn valde: " + cpuplayermove);
        System.out.println();
        return cpuplayermove;
    }

    public static String getHumanPlayerMove(){
       Scanner n = new Scanner(System.in);
        String input = n.next();
        String humanplayermove = input.toUpperCase();
        System.out.println("Du valde: " + humanplayermove);
        return humanplayermove;
    }
}